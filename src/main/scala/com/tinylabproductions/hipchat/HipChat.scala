package com.tinylabproductions.hipchat

import play.api.libs.json.{Json, JsValue}
import com.ning.http.client.RequestBuilder
import dispatch._

object HipChat {

  // https://www.hipchat.com/docs/apiv2/method/send_room_notification
  val maxMessageLength = 9900

  @inline private[this] def notificationToUrl
  (apiUrl: String, notification: Notification) = {
    url(s"$apiUrl/${notification.roomId}/notification")
      .addQueryParameter("auth_token", notification.authToken)
  }

  implicit class RequestBuilderExts(val rb: RequestBuilder) extends AnyVal {
    def postJson(js: JsValue) = {
      rb.POST.addHeader("Content-Type", "application/json") << js.toString()
    }
  }

  def send(n: Notification)(implicit logger: Logger) {
    val request = Json.obj(
      "color" -> n.color,
      "message_format" -> "html",
      "message" -> {
        if (n.message.length > maxMessageLength)
          n.message.take(maxMessageLength) + "..."
        else n.message
      }
    )
    Http(notificationToUrl(n.apiUrl, n) postJson request).onComplete {
      case Right(response) =>
        logger.debug(s"After posting hipchat notification got status ${
          response.getStatusCode} and body: ${response.getResponseBody}")
      case Left(ex) =>
        logger.debug(s"Failure while posting hipchat notification: $ex")
    }
  }

  case class Notification(
    apiUrl: String,
    roomId: Int,
    authToken: String,
    color: String,
    message: String
  )
}
