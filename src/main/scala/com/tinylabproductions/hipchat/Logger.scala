package com.tinylabproductions.hipchat

/**
 * Trait to be mixed in into your app logger class or object.
 */
trait Logger {
  def error(msg: => String)
  def error(msg: => String, ex: => Throwable)
  def info(msg: => String)
  def debug(msg: => String)
}
