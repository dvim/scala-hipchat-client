import sbt._
import sbt.Keys._

object HipChatClientBuild extends Build {
  lazy val hipChatClient = Project(
    id = "hipchat-client",
    base = file("."),
    settings = Project.defaultSettings ++ Seq(
      name := "HipChat Client",
      organization := "com.tinylabproductions",
      version := "1.0",
      scalaVersion := "2.10.2",
      scalaBinaryVersion := "2.10",
      resolvers ++= Seq(
        "Typesafe Releases" at "http://repo.typesafe.com/typesafe/releases/"
      ),
      libraryDependencies ++= Seq(
        "com.typesafe.play"       %% "play-json" % "2.2.0",
        // Using older than 0.10.1 here, because new one requires newer netty
        // and that produces errors with play 2.1.0/2.1.1.
        "net.databinder.dispatch" %% "dispatch-core" % "0.9.5",

        "org.scalautils" % "scalautils_2.10" % "2.0",
        "org.scalatest" %% "scalatest" % "2.0" % "test"
      )
    )
  )
}
